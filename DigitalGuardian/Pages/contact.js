var until               = protractor.ExpectedConditions;
var frmContactUs        = element(by.id("mktoForm_201"));
var txtFirstName        = element(by.id("FirstName"));
var txtLastName         = element(by.id("LastName"));
var txtEmail            = element(by.id("Email"));
var txtCompany          = element(by.id("Company"));
var txtJobTitle         = element(by.id("Title"));
var txtPhone            = element(by.id("Phone"));
var cboCountry          = element(by.id("Country"));
var cboState            = element(by.id("State"));
var btnSubmit           = element(by.css("#mktoForm_201 > div.mktoButtonRow > span > button"));
var utahOfficeInfo      = element(by.id("locationsUTTitle"));

// for simplicity of number of files
var mediaIcon           = element(by.id("facebookIcon"));


module.exports = {

    fillForm: async function( fname, lname, email ) {
        await browser.wait(until.visibilityOf(frmContactUs, 5000, "Contact us form did not load."));
        await txtFirstName.sendKeys(fname);
        await txtLastName.sendKeys(lname);
        await txtEmail.sendKeys(email);
        await txtCompany.sendKeys("Wile E Coyote Acme Co.");
        //await txtJobTitle.sendKeys("QA Engineer Candidate");
        await txtPhone.sendKeys("8019991234");
        await cboCountry.click();
        await element(by.css("#Country > option[value='United States']")).click();
        await cboState.click();
        await element(by.css("#State > option[value='GA']")).click();
   },

    clickSubmit: async function() {
        //await btnSubmit.click();
    },

    utahInfo: async function() {
        return utahOfficeInfo.getText();
    }

} // end module

