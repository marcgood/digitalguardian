var until               = protractor.ExpectedConditions;
var navContact          = element(by.css("#menu > ul.nav.navbar-nav.navbar-right.nav-utility > li.first.leaf.link--535416.contact"));
/*
var navSupport
var navContact          
var navPartners
var navLanguage
var navSearch
*/

module.exports = {

    goToContact: async function() {
        await navContact.click();
    }

} // end module