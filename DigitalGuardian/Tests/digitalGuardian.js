// Page Objects in use
var home        = require('../Pages/home.js');
var contact     = require('../Pages/contact.js');
var support     = require('../Pages/support.js'); 
var blog	      = require('../Pages/blog.js');
var partners    = require('../Pages/partners.js');

var homePageTitle   = "Enterprise IP & DLP Software | Digital Guardian";

describe('Acceptance Tests', function() {
  // variables to use in test
  var firstName     = 'Marc';
  var lastName      = 'Good';
  var email         = 'marc_good@hotmail.com';
  var utahAddress   = '3130 West Maple Loop Drive, Suite 110';

  beforeEach( function() {
    browser.ignoreSynchronization = true;
    browser.get('https://www.digitalguardian.com');
  });

  // Test #1 - Title
  // Checkk the site's home page even loads
    it('should navigate to home page', async function() {
      expect(browser.getTitle()).toEqual(homePageTitle);
    });

  // Test #2 - Fill form on Contact
  // Confirm login process works and loads account
    it('should open Contact Us page', async function() {
      await home.goToContact();
      await contact.fillForm(firstName, lastName, email)
      await contact.clickSubmit();
      expect(await contact.utahInfo()).toContain(utahAddress);
  });


}); // end describe