exports.config = {
  framework: 'jasmine2',
  seleniumAddress: 'http://localhost:4444/wd/hub',
    capabilities: {
    'browserName': 'chrome'  // Sign into acct will not work with Chrome
    //'browserName': 'firefox'
    //'chromeOptions': {
    //  'args': ['--disable-web-security', '--user-data-dir']
  },
  specs: ['Tests/digital*.js']
  //directConnect: true
}
